﻿



namespace PcSearchEngine
{
    partial class FormPcSearchEngine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPcSearchEngine));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.grp_dir = new System.Windows.Forms.GroupBox();
            this.btnExcelSearchResult = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btn_browse = new System.Windows.Forms.Button();
            this.txt_directory = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grp_docList = new System.Windows.Forms.GroupBox();
            this.lv_document = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonExcelOccurence = new System.Windows.Forms.Button();
            this.listViewOccurence = new System.Windows.Forms.ListView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grp_dir.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grp_docList.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_dir
            // 
            this.grp_dir.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.grp_dir.Controls.Add(this.btnExcelSearchResult);
            this.grp_dir.Controls.Add(this.progressBar1);
            this.grp_dir.Controls.Add(this.btn_browse);
            this.grp_dir.Controls.Add(this.txt_directory);
            this.grp_dir.Dock = System.Windows.Forms.DockStyle.Top;
            this.grp_dir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grp_dir.Location = new System.Drawing.Point(3, 3);
            this.grp_dir.Name = "grp_dir";
            this.grp_dir.Size = new System.Drawing.Size(1204, 94);
            this.grp_dir.TabIndex = 1;
            this.grp_dir.TabStop = false;
            this.grp_dir.Text = "Directory Selection";
            // 
            // btnExcelSearchResult
            // 
            this.btnExcelSearchResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExcelSearchResult.Image = ((System.Drawing.Image)(resources.GetObject("btnExcelSearchResult.Image")));
            this.btnExcelSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcelSearchResult.Location = new System.Drawing.Point(455, 52);
            this.btnExcelSearchResult.Name = "btnExcelSearchResult";
            this.btnExcelSearchResult.Size = new System.Drawing.Size(140, 25);
            this.btnExcelSearchResult.TabIndex = 0;
            this.btnExcelSearchResult.Text = "Export to Excel";
            this.btnExcelSearchResult.UseVisualStyleBackColor = true;
            this.btnExcelSearchResult.Click += new System.EventHandler(this.btnExcelSearchResult_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(7, 52);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(439, 22);
            this.progressBar1.TabIndex = 1;
            // 
            // btn_browse
            // 
            this.btn_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_browse.Location = new System.Drawing.Point(455, 21);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(140, 25);
            this.btn_browse.TabIndex = 1;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // txt_directory
            // 
            this.txt_directory.Enabled = false;
            this.txt_directory.Location = new System.Drawing.Point(7, 21);
            this.txt_directory.Name = "txt_directory";
            this.txt_directory.Size = new System.Drawing.Size(439, 20);
            this.txt_directory.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1218, 675);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage1.Controls.Add(this.grp_docList);
            this.tabPage1.Controls.Add(this.grp_dir);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1210, 649);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Search";
            // 
            // grp_docList
            // 
            this.grp_docList.Controls.Add(this.lv_document);
            this.grp_docList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grp_docList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grp_docList.Location = new System.Drawing.Point(3, 97);
            this.grp_docList.Name = "grp_docList";
            this.grp_docList.Size = new System.Drawing.Size(1204, 549);
            this.grp_docList.TabIndex = 3;
            this.grp_docList.TabStop = false;
            this.grp_docList.Text = "Document List";
            // 
            // lv_document
            // 
            this.lv_document.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_document.Location = new System.Drawing.Point(3, 16);
            this.lv_document.Name = "lv_document";
            this.lv_document.Size = new System.Drawing.Size(1198, 530);
            this.lv_document.TabIndex = 2;
            this.lv_document.UseCompatibleStateImageBehavior = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage2.Controls.Add(this.buttonExcelOccurence);
            this.tabPage2.Controls.Add(this.listViewOccurence);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1210, 649);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "List of Occurences";
            // 
            // buttonExcelOccurence
            // 
            this.buttonExcelOccurence.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonExcelOccurence.Image = ((System.Drawing.Image)(resources.GetObject("buttonExcelOccurence.Image")));
            this.buttonExcelOccurence.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonExcelOccurence.Location = new System.Drawing.Point(445, 19);
            this.buttonExcelOccurence.Name = "buttonExcelOccurence";
            this.buttonExcelOccurence.Size = new System.Drawing.Size(140, 25);
            this.buttonExcelOccurence.TabIndex = 5;
            this.buttonExcelOccurence.Text = "Export to Excel";
            this.buttonExcelOccurence.UseVisualStyleBackColor = true;
            this.buttonExcelOccurence.Click += new System.EventHandler(this.buttonExcelOccurence_Click);
            // 
            // listViewOccurence
            // 
            this.listViewOccurence.Dock = System.Windows.Forms.DockStyle.Left;
            this.listViewOccurence.Location = new System.Drawing.Point(3, 3);
            this.listViewOccurence.Name = "listViewOccurence";
            this.listViewOccurence.Size = new System.Drawing.Size(413, 643);
            this.listViewOccurence.TabIndex = 4;
            this.listViewOccurence.UseCompatibleStateImageBehavior = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormPcSearchEngine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1218, 675);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPcSearchEngine";
            this.Text = "PC Search Engine";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grp_dir.ResumeLayout(false);
            this.grp_dir.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.grp_docList.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox grp_dir;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.TextBox txt_directory;
        private System.Windows.Forms.ListView lv_document;
        private System.Windows.Forms.GroupBox grp_docList;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnExcelSearchResult;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listViewOccurence;
        private System.Windows.Forms.Button buttonExcelOccurence;
    }
}
