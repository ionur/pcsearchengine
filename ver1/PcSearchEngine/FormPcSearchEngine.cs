﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace PcSearchEngine
{
    public partial class FormPcSearchEngine : Form
    {
        int maxDepth = 0;
        int first = 0;
        bool depth0 = true;
        string occurence = "";
        string[] numberOfOccurence;
        FolderBrowserDialog fBrowser;


        public FormPcSearchEngine()
        {
            InitializeComponent();
            progressBar1.Minimum = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar1.Value = progressBar1.Minimum;
            progressBar1.Visible = false;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {

            progressBar1.Visible = false;
            progressBar1.Value = progressBar1.Minimum;
            fBrowser = new FolderBrowserDialog();

            fBrowser.ShowDialog();
            txt_directory.Text = fBrowser.SelectedPath;

            if (fBrowser.SelectedPath != String.Empty)
            {
                ListDirectory(fBrowser.SelectedPath);
                ListOccurence();
            }


        }

        private void ListDirectory(string directory)
        {
            maxDepth = 0;
            first = 0;
            this.lv_document.Clear();
            this.lv_document.Columns.Clear();
            this.lv_document.View = View.Details;
            this.lv_document.FullRowSelect = true;
            this.lv_document.GridLines = true;

            FindMaxDepth(directory, 0);

            progressBar1.Visible = true;
            Search(directory, 0);

            CreateHeadersAndFillListView(maxDepth);
        }

        private void ListOccurence()
        {
            //dosya isimlerindeki istenmeyen karakterleri kaldırır, boşlukları düzeltir
            occurence = Regex.Replace(occurence, @"\W+", " ");
            occurence = occurence.Replace('_', ' ');
            occurence = occurence.Replace('-', ' ');
            occurence = occurence.Replace("  ", " ");
            occurence = occurence.Replace("   ", " ");
            occurence = occurence.Replace("    ", " ");
            numberOfOccurence = occurence.Split(' ');

            //dosya isimlerinde geçen her kelimeyi listeye alır, gereksiz elemanları kaldırır
            List<string> list = new List<string>(numberOfOccurence);
            list.Sort();

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Length == 0 || list[i].Length == 1 || list[i].Length == 2)
                {
                    list.RemoveAt(i);
                    i = 0;
                }

                list[i] = list[i].ToLower();
                list[i] = list[i].Replace('ı', 'i');
            }

            string[] stopWords = {"un",	"por",	"ets",	"aren't",	"he'd",	"desde",	"saber",	"la",	"she'll",	"what",
                                "una",	"poder",	"som",	"as",	"he'll",	"conseguir",	"sabes",	"les",	"she's",	"what's",
                                "unas",	"puede",	"estic",	"at",	"he's",	"consigo",	"sabe",	"els",	"should",	"when",
                                "unos",	"puedo",	"està",	"be",	"her",	"consigue",	"sabemos",	"seu",	"shouldn't",	"when's",
                                "uno",	"podemos",	"estem",	"because",	"here",	"consigues",	"sabeis",	"aquí",	"so",	"where",
                                "sobre",	"podeis",	"esteu",	"been",	"here's",	"conseguimos",	"saben",	"meu",	"some",	"where's",
                                "todo",	"pueden",	"estan",	"before",	"hers",	"consiguen",	"ultimo",	"teu",	"such",	"which",
                                "también",	"fui",	"com",	"being",	"herself",	"ir",	"largo",	"ells",	"than",	"while",
                                "tras",	"fue",	"en",	"below",	"him",	"voy",	"bastante",	"elles",	"that",	"who",
                                "otro",	"fuimos",	"per",	"between",	"himself",	"va",	"haces",	"ens",	"that's",	"who's",
                                "algún",	"fueron",	"perquè",	"both",	"his",	"vamos",	"muchos",	"nosaltres",	"the",	"whom",
                                "alguno",	"hacer",	"per que",	"but",	"how",	"vais",	"aquellos",	"vosaltres",	"their",	"why",
                                "alguna",	"hago",	"estat",	"by",	"how's",	"van",	"aquellas",	"si",	"theirs",	"why's",
                                "algunos",	"hace",	"estava",	"can't",	"i",	"vaya",	"sus",	"dins",	"them",	"with",
                                "algunas",	"hacemos",	"ans",	"cannot",	"i'd",	"gueno",	"entonces",	"sols",	"themselves",	"won't",
                                "ser",	"haceis",	"abans",	"could",	"i'll",	"ha",	"tiempo",	"solament",	"then",	"would",
                                "es",	"hacen",	"éssent",	"couldn't",	"i'm",	"tener",	"verdad",	"saber",	"there",	"wouldn't",
                                "soy",	"cada",	"ambdós",	"did",	"i've",	"tengo",	"verdadero",	"saps",	"there's",	"you",
                                "eres",	"fin",	"però",	"didn't",	"if",	"tiene",	"verdadera",	"sap",	"these",	"you'd",
                                "somos",	"incluso",	"per",	"do",	"in",	"tenemos",	"faig",	"sabem",	"they",	"you'll",
                                "sois",	"primero",	"poder",	"does",	"into",	"teneis",	"fa",	"sabeu",	"they'd",	"you're",
                                "estoy",	"de",	"potser",	"doesn't",	"is",	"tienen",	"fem",	"saben",	"they'll",	"you've",
                                "esta",	"es",	"puc",	"doing",	"isn't",	"el",	"feu",	"últim",	"they're",	"your",
                                "estamos",	"i",	"podem",	"don't",	"it",	"la",	"fan",	"llarg",	"they've",	"yours",
                                "estais",	"a",	"podeu",	"down",	"it's",	"lo",	"cada",	"bastant",	"this",	"yourself",
                                "estan",	"o",	"poden",	"during",	"its",	"las",	"fi",	"consigueix",	"those",	"yourselves",
                                "como",	"un",	"vaig",	"each",	"itself",	"los",	"inclòs",	"consigueixes",	"through",	"cierto",
                                "en",	"una",	"va",	"few",	"let's",	"su",	"primer",	"conseguim",	"to",	"ciertos",
                                "para",	"unes",	"van",	"for",	"me",	"aqui",	"des de",	"consigueixen",	"too",	"cierta",
                                "atras",	"uns",	"fer",	"from",	"more",	"mio",	"conseguir",	"anar",	"under",	"ciertas",
                                "porque",	"un",	"a",	"further",	"most",	"tuyo",	"consegueixo",	"intento",	"until",	"intentar",
                                "por qué",	"tot",	"about",	"had",	"mustn't",	"ellos",	"was",	"very",	"up",	"ellas",
                                "estado",	"també",	"above",	"hadn't",	"my",	"intenta",	"podrias",	"amb",	"sota",	"quan",
                                "estaba",	"altre",	"after",	"has",	"myself",	"intentas",	"podriamos",	"entre",	"dalt",	"on",
                                "ante",	"algun",	"again",	"hasn't",	"no",	"intentamos",	"podrian",	"sense",	"ús",	"mentre",
                                "antes",	"alguna",	"against",	"have",	"nor",	"intentais",	"podriais",	"jo",	"molt",	"qui",
                                "siendo",	"alguns",	"all",	"haven't",	"not",	"intentan",	"yo",	"aquell",	"era",	"mientras",
                                "ambos",	"algunes",	"am",	"having",	"of",	"dos",	"aquel",	"trabajar",	"eres",	"quien",
                                "pero",	"ser",	"an",	"he",	"off",	"bajo",	"fas",	"trabajas",	"erem",	"con",
                                "soc",	"és",	"and",	"any",	"are",	"arriba",	"molts",	"trabaja",	"eren",	"entre",
                                "on",	"nos",	"haver",	"ourselves",	"wasn't",	"encima",	"aquells",	"trabajamos",	"mode",	"sin",
                                "once",	"nosotros",	"tenir",	"out",	"we",	"usar",	"aquelles",	"trabajais",	"bé",	"trabajo",
                                "only",	"vosotros",	"tinc",	"over",	"we'd",	"uso",	"seus",	"trabajan",	"quant",	"emplear",
                                "or",	"vosotras",	"te",	"own",	"we'll",	"usas",	"llavors",	"podria",	"emplean",	"empleo",
                                "other",	"si",	"tenim",	"same",	"we're",	"usa",	"era",	"modo",	"ampleamos",	"empleas",
                                "ought",	"dentro",	"teniu",	"shan't",	"we've",	"usamos",	"eras",	"bien",	"empleais",	"donde",
                                "our",	"solo",	"tene",	"she",	"were",	"usais",	"eramos",	"cual",	"valor",	"muy",
                                "ours ",	"solamente",	"el",	"she'd",	"weren't",	"usan",	"eran",	"cuando",		
                                };

            for (int i = 0; i < list.Count; i++)
            {
                for(int k=0; k<stopWords.Length; k++)
                {
                    if (list[i].ToString() == stopWords[k].ToString())
                        list.RemoveAt(i);
                }
            }

            //listview oluşturur
            this.listViewOccurence.Clear();
            this.listViewOccurence.Columns.Clear();
            this.listViewOccurence.View = View.Details;
            this.listViewOccurence.FullRowSelect = true;
            this.listViewOccurence.GridLines = true;

            ColumnHeader colHeadOccurence;

            //kolonları oluşturuyor ama 141.satırda kelimeleri 'Word' kolonuna, 142.satırda count ları 'Occurence' satırına eklemeye çalışıyorum ama olmuyor aq, bi tane daha 'asd' diye kolon ekledim anca oldu. o ilk kolon boşa gidiyo lan. bi de listeyi count a göre nasıl sıralarız? büyükten küçüğe
            colHeadOccurence = new ColumnHeader();
            colHeadOccurence.Width = 120;
            colHeadOccurence.Text = "";
            this.listViewOccurence.Columns.Add(colHeadOccurence);

            colHeadOccurence = new ColumnHeader();
            colHeadOccurence.Width = 120;
            colHeadOccurence.Text = "Word";
            this.listViewOccurence.Columns.Add(colHeadOccurence);

            colHeadOccurence = new ColumnHeader();
            colHeadOccurence.Width = 120;
            colHeadOccurence.Text = "Occurence";
            this.listViewOccurence.Columns.Add(colHeadOccurence);

            int count = 0; //kelimenin kaç kere geçtiğini tutar
            int iterator = 0;//liste üzerinde nerden itibaren karşılaştırma yapılması gerektiğini hesaplar
            int j = 1;//iterator ile karşılaştırılması gereken elemanın konumunu tutar

            for (iterator = 0; iterator < list.Count - 1; )
            {
                if (list[iterator].ToString() == list[j].ToString())
                {
                    count++;
                    j++;
                }
                else
                {
                    //iterator elemanı ile bir sonraki eşit değilse kelimenin kendisini ve kaç kere geçtiğini listeye atar
                    ListViewItem item = new ListViewItem();
                    item.SubItems.Add(list[iterator].ToString());
                    item.SubItems.Add((count + 1).ToString());
                    listViewOccurence.Items.Add(item);
                    iterator += count + 1;
                    j++;
                    count = 0;
                }
            }

            listViewOccurence.Sorting = SortOrder.Descending;
        }

        private void CreateHeadersAndFillListView(int depth)
        {
            ColumnHeader colHead;

            for (int i = 0; i < depth + 1; i++)
            {
                colHead = new ColumnHeader();
                colHead.Width = 150;
                colHead.Text = "Name of file or folder";
                this.lv_document.Columns.Add(colHead);
            }

            colHead = new ColumnHeader();
            colHead.Width = 120;
            colHead.Text = "Date of creation";
            this.lv_document.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Width = 120;
            colHead.Text = "Year of creation";
            this.lv_document.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Width = 150;
            colHead.Text = "Date of last modification";
            this.lv_document.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Width = 150;
            colHead.Text = "Year of last modification";
            this.lv_document.Columns.Add(colHead);

            for (int i = 1; i < depth + 1; i++)
            {
                colHead = new ColumnHeader();
                colHead.Width = 130;
                colHead.Text = i + " level of folder";
                this.lv_document.Columns.Add(colHead);
            }

            colHead = new ColumnHeader();
            colHead.Width = 200;
            colHead.Text = "Title of the file";
            this.lv_document.Columns.Add(colHead);



            colHead = new ColumnHeader();
            colHead.Width = 110;
            colHead.Text = "Type of archive";
            this.lv_document.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Width = 110;
            colHead.Text = "Weight of the file";
            this.lv_document.Columns.Add(colHead);
        }

        private void FindMaxDepth(string directory, int depth)
        {
            if (maxDepth < depth)
            {
                maxDepth = depth;
            }

            string[] dirFolder = Directory.GetDirectories(directory);

            if (dirFolder.Length != 0)
            {
                depth++;
                foreach (string folder in dirFolder)
                {
                    try
                    {
                        FindMaxDepth(folder, depth);
                    }

                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                }
            }

            progressBar1.Maximum = maxDepth;
        }

        private void Search(string directory, int depth)
        {
            progressBar1.PerformStep();
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            string folderName = dirInfo.Name;

            ListViewItem item;
            if (depth == 0)
            {
                item = new ListViewItem(folderName);
            }
            else
            {
                item = new ListViewItem();
                for (int y = 1; y < depth; y++)
                {
                    item.SubItems.Add(" ");
                }
                item.SubItems.Add(folderName);
            }

            this.lv_document.Items.Add(item);

            string[] dirFolder = Directory.GetDirectories(directory);
            if (dirFolder.Length != 0)
            {
                depth0 = false;
                depth++;
                foreach (string folder in dirFolder)
                {
                    Search(folder, depth);

                    string[] dirDocument = Directory.GetFiles(folder);
                    foreach (string doc in dirDocument)
                    {
                        if (first == 0)
                        {
                            maxDepth++;
                        }
                        first++;

                        FileInfo fileInfo = new FileInfo(doc);
                        DateTime createDate = fileInfo.CreationTime;
                        DateTime modifyDate = fileInfo.LastWriteTime;
                        string fileLength = (Convert.ToDouble(fileInfo.Length / 1024) + 1).ToString();

                        if (doc.EndsWith(".xlsx") || doc.EndsWith(".docx") ||
                            doc.EndsWith(".dotx") || doc.EndsWith(".doc") ||
                            doc.EndsWith(".xls") || doc.EndsWith(".txt") ||
                            doc.EndsWith(".jpg") || doc.EndsWith(".jpeg") ||
                            doc.EndsWith(".pdf") || doc.EndsWith(".ppt"))
                        {
                            item = new ListViewItem();
                            for (int y = 1; y < depth + 1; y++)
                            {
                                item.SubItems.Add(" ");
                            }
                            item.SubItems.Add(fileInfo.Name);

                            for (int y = 1; y < maxDepth - depth; y++)
                            {
                                item.SubItems.Add(" ");
                            }

                            item.SubItems.Add(createDate.ToString("dd.MM.yy"));
                            item.SubItems.Add(createDate.ToString("yyyy"));
                            item.SubItems.Add(modifyDate.ToString("dd.MM.yy"));
                            item.SubItems.Add(modifyDate.ToString("yyyy"));

                            DirectoryInfo selectedDir = new DirectoryInfo(fBrowser.SelectedPath);
                            item.SubItems.Add(selectedDir.Name);

                            string path = String.Empty;
                            path = fileInfo.Directory.ToString().Replace(fBrowser.SelectedPath + '\\', "");

                            string[] words = path.Split('\\');
                            foreach (string word in words)
                            {
                                item.SubItems.Add(word);
                            }

                            for (int y = 1; y < maxDepth - depth; y++)
                            {
                                item.SubItems.Add(" ");
                            }

                            string[] names = fileInfo.Name.Split('.');
                            string fileName = "";
                            string fileType = "";
                            if (names.Length > 2)
                            {
                                fileType = names[names.Length - 1];

                                for (int i = 0; i < names.Length - 1; i++)
                                {
                                    fileName += names[i];
                                    if (i != names.Length - 2)
                                        fileName += ".";
                                }

                                item.SubItems.Add(fileName);
                                occurence += fileName + " ";
                                item.SubItems.Add(fileType);
                            }

                            else
                            {
                                foreach (string name in names)
                                {
                                    item.SubItems.Add(name);
                                    //occurence += name + " ";
                                }
                                occurence += names[0] + " ";
                            }

                            item.SubItems.Add(fileLength + " Kb");
                            lv_document.Items.Add(item);
                        }
                    }
                }
            }
            else if (depth0)
            {
                // Depth = 0 
                string[] dirDocument = Directory.GetFiles(directory);
                foreach (string doc in dirDocument)
                {
                    if (first == 0)
                    {
                        maxDepth++;
                    }
                    first++;

                    FileInfo fileInfo = new FileInfo(doc);
                    DateTime createDate = fileInfo.CreationTime;
                    DateTime modifyDate = fileInfo.LastWriteTime;
                    string fileLength = (Convert.ToDouble(fileInfo.Length / 1024) + 1).ToString();

                    if (doc.EndsWith(".xlsx") || doc.EndsWith(".docx") ||
                        doc.EndsWith(".dotx") || doc.EndsWith(".doc") ||
                        doc.EndsWith(".xls") || doc.EndsWith(".txt") ||
                        doc.EndsWith(".pdf") || doc.EndsWith(".ppt"))
                    {
                        item = new ListViewItem();
                        for (int y = 1; y < depth + 1; y++)
                        {
                            item.SubItems.Add(" ");
                        }
                        item.SubItems.Add(fileInfo.Name);

                        for (int y = 1; y < maxDepth - depth; y++)
                        {
                            item.SubItems.Add(" ");
                        }

                        item.SubItems.Add(createDate.ToString("dd.MM.yy"));
                        item.SubItems.Add(createDate.ToString("yyyy"));
                        item.SubItems.Add(modifyDate.ToString("dd.MM.yy"));
                        item.SubItems.Add(modifyDate.ToString("yyyy"));

                        DirectoryInfo selectedDir = new DirectoryInfo(fBrowser.SelectedPath);
                        item.SubItems.Add(selectedDir.Name);

                        string[] names = fileInfo.Name.Split('.');
                        string fileName = "";
                        string fileType = "";
                        if (names.Length > 2)
                        {
                            fileType = names[names.Length - 1];

                            for (int i = 0; i < names.Length - 1; i++)
                            {
                                fileName += names[i];
                                if (i != names.Length - 2)
                                    fileName += ".";
                            }

                            item.SubItems.Add(fileName);
                            occurence += fileName + " ";
                            item.SubItems.Add(fileType);
                        }

                        else
                        {
                            foreach (string name in names)
                            {
                                item.SubItems.Add(name);
                                //occurence += name + " ";
                            }
                            occurence += names[0] + " ";
                        }

                        item.SubItems.Add(fileLength + " Kb");
                        lv_document.Items.Add(item);
                    }
                }
            }
        }

        private void DocumentListToExcel()
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            excel.Visible = true;

            Microsoft.Office.Interop.Excel.Workbook wb = excel.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];

            #region Excel information
            for (int x = 1; x < maxDepth + 2; x++)
            {
                ws.Cells[1, x] = "Name of file or folder";
            }

            ws.Cells[1, maxDepth + 2] = "Date of creation";

            ws.Cells[1, maxDepth + 3] = "Year of creation";

            ws.Cells[1, maxDepth + 4] = "Date of last modification";

            ws.Cells[1, maxDepth + 5] = "Year of last modification";

            for (int y = 1; y < maxDepth + 2; y++)
            {
                ws.Cells[1, y + maxDepth + 5] = y + " level of folder";
            }

            ws.Cells[1, maxDepth + maxDepth + 6] = "Title of the file";

            ws.Cells[1, maxDepth + maxDepth + 7] = "Type of archive";

            ws.Cells[1, maxDepth + maxDepth + 8] = "Weight of the file";
            #endregion

            int i = 1;
            int i2 = 2;

            foreach (ListViewItem lvi in lv_document.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    i++;
                }
                i2++;
            }
        }

        private void OccurenceToExcel()
        {
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 1;
            foreach (ListViewItem lvi in listViewOccurence.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    i++;
                }
                i2++;
            }
        }

        private void btnExcelSearchResult_Click(object sender, EventArgs e)
        {
            if (lv_document.Items.Count != 0)
            {
                DocumentListToExcel();
            }
            else
            {
                MessageBox.Show("Please select a document.", "Information");
            }
        }

        private void buttonExcelOccurence_Click(object sender, EventArgs e)
        {
            if (listViewOccurence.Items.Count != 0)
            {
                OccurenceToExcel();
            }
            else
            {
                MessageBox.Show("Please select a document.", "Information");
            }
        }
    }
}